﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scaling : MonoBehaviour
{
	[SerializeField]
	private Vector3 axes;
	public float scaleUnits;

    // Update is called once per frame
    void Update()
    {
		axes = TransformMov.ClampVector3(axes);
		transform.localScale += axes * (scaleUnits * Time.deltaTime);
    }
}
